-- list down all the databases inside the DBMS

SHOW DATABASES;

-- Create a database.
-- Syntax:
	-- CREATE DATABASE <name_db>

CREATE DATABASE music_db;
CREATE DATABASE movies_db;


-- DROP DATABASE, Deletion of database
-- Syntax: 
	-- DROP DATABASE <name_db>

DROP DATABASE movies_db;

-- SELECT a database.
-- Syntax 
	-- USE <name_db>
USE music_db;

-- CREATE tables.
-- Table columns will also be declared here
CREATE TABLE users (
	-- INT to indicate that the id column will have integer entry
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	contact_number CHAR(11)  NULL,
	email VARCHAR(50) NOT NULL,
	address VARCHAR(50),
	-- To set the primary key
	PRIMARY KEY (id)
);

-- Drop table from database
DROP TABLE userss;

CREATE TABLE playslist (
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	playslist_name VARCHAR(30) NOT NULL,
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlist_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE artist (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);

-- Mini-Activity for albums
-- columns: id, album_title, date_released, artist_id


CREATE TABLE albums (
	id INT NOT NULL	AUTO_INCREMENT,
	artist_id INT NOT NULL,
	album_title VARCHAR(50) NOT NULL,
	date_released DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_albums_artist_id
		FOREIGN KEY (artist_id) REFERENCES artist(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);




